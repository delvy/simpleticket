<?php

namespace App\Tests\Builder;


use App\Domain\User\Registered;

class RegisteredBuilder extends UserBuilder
{
	public function build()
	{
		$user = new Registered(
			$this->id,
			$this->email,
			$this->roles
		);
		$user->updatePassword($this->password);
		return $user;
	}
}