<?php

namespace App\Tests\Builder;

use App\Domain\Ticket\Ticket;
use App\Domain\User\Admin;
use App\Domain\User\User;

class TicketBuilder
{

	private $id;
	private $createdAt;
	private $updatedAt;
	private $status;
	private $owner;
	private $assignee;
	private $messages;

	/**
	 * TicketBuilder constructor.
	 *
	 * @param $id
	 * @param $createdAt
	 * @param $updatedAt
	 * @param $status
	 * @param $owner
	 * @param $assignee
	 * @param $messages
	 */
	public function __construct()
	{
		$this->id        = 1;
		$this->createdAt = new \DateTime();
		$this->updatedAt = new \DateTime();
		$this->status    = Ticket::NEW;
		$this->owner     = (new RegisteredBuilder())->build();
		$this->assignee  = null;
		$this->messages  = [(new MessageBuilder())->build()];
	}

	public function withOwner(User $user)
	{
		$this->owner = $user;
		return $this;
	}

	public function withAssignee(Admin $admin)
	{
		$this->assignee = $admin;
		return $this;
	}

	public function build()
	{
		$ticket = new Ticket(
			$this->id,
			$this->createdAt,
			$this->status,
			$this->owner
		);
		if(!empty($this->assignee)){
			$ticket->assign($this->assignee);
		}
		foreach ($this->messages as $message) {
			$ticket->addMessage($message->getBody(), $this->owner);
		}

		return $ticket;
	}
}