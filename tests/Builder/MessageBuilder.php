<?php

namespace App\Tests\Builder;


use App\Domain\Ticket\Message;

class MessageBuilder
{
	private $id;
	private $createdAt;
	private $author;
	private $body;
	private $number;
	private $ticket;

	/**
	 * Message constructor.
	 *
	 * @param $id
	 * @param $author
	 * @param $body
	 * @param $number
	 */
	public function __construct()
	{
		$this->id     = '1234';
		$this->author = (new RegisteredBuilder())->build();
		$this->body   = 'Testo del builder';
		$this->number = 1;
		$this->createdAt = new \DateTime();
	}

	public function build()
	{
		$message = new 	Message(
			$this->id,
			$this->author,
			$this->body,
		    $this->number);
		return $message;
	}
}