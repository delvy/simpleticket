<?php

namespace App\Tests\Builder;


abstract class UserBuilder
{
	protected $id;
	protected $email;
	protected $password;
	protected $roles = [];

	/**
	 * UserBuilder constructor.
	 *
	 * @param $id
	 */
	public function __construct()
	{
		$this->id = '11111111-1234-1234-1234-111111111111';
		$this->email = 'test@test.it';
		$this->roles = ["ROLE_USER"];
		$this->password = 'abcde';
	}

	public function withId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function withEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	public function withRoles()
	{
		$this->roles;
		return $this;
	}

	public function withPassword()
	{
		$this->password;
		return $this;
	}

	abstract function build();
}