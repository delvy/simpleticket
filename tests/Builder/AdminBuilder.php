<?php

namespace App\Tests\Builder;


use App\Domain\User\Admin;

class AdminBuilder extends UserBuilder
{

	public function build()
	{
		$user = new Admin(
			$this->id,
			$this->email,
			$this->roles
		);
		$user->updatePassword($this->password);
		return $user;
	}
}