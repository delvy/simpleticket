<?php

namespace App\Tests\Domain\Command;


use App\Domain\Command\Assign;
use App\Domain\Command\Take;
use App\Domain\Command\TicketValidator;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;

class TakeTest extends TicketCommandTest
{
	/** @var Assign */
	private $takeCommand;

	public function setUp()
	{
		parent::setUp();
		$this->takeCommand = new Take($this->fakeTicketRepository, new TicketValidator(), $this->fakeDispatcher);
	}


	public function test_whenTake_itUpdateTicketAssigee()
	{
		$this->takeCommand->execute('1', $this->userAdminA);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));
		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */
		$this->assertEquals('adminA',$capturedTicket->getAssigneeName());
	}

	public function test_whenTake_withNoNewTicket_itRaisesException()
	{
		$this->expectException(ModifyNotAllowedException::class);

		$ticket = $this->fakeTicketRepository->findById(1);
		$ticket->assign($this->userAdminA);
		$this->takeCommand->execute('1', $this->userAdminA);
	}

	public function test_whenTake_withNullUser_itReportError()
	{
		$this->expectException(\Error::class);
		$this->takeCommand->execute('1', null);
	}

	public function test_whenTake_withNotAdminUser_itReportError()
	{
		$this->expectException(\Error::class);
		$this->takeCommand->execute('1', $this->userRegistered);
	}
}