<?php

namespace App\Tests\Domain\Command;


use App\Domain\Command\TicketValidator;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Exception\ReadNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Tests\Builder\AdminBuilder;
use App\Tests\Builder\RegisteredBuilder;
use App\Tests\Builder\TicketBuilder;
use PHPUnit\Framework\TestCase;

class TicketValidatorTest extends TestCase
{
	/** @var Ticket */
	public $ticket;
	public $defaultUser;
	public $anotherUser;
	public $admin;
	public $anotherAdmin;

	public function setup()
	{
		$this->defaultUser = (new RegisteredBuilder())
			->build();
		$this->anotherUser = (new RegisteredBuilder())
			->withId('another')
			->build();
		$this->admin = (new AdminBuilder())
			->build();
		$this->anotherAdmin = (new AdminBuilder())
			->withId('anotherAdmin')
			->build();

		$this->ticket = (new TicketBuilder())
			->withOwner($this->defaultUser)
			->build();
	}

	public function test_withTicketOwnedByUser_notRaisesException()
	{
		$validator = new TicketValidator();
		try {
			$validator->assertIfUserCanRead($this->ticket, $this->defaultUser);
			$validator->assertIfUserCanModify($this->ticket, $this->defaultUser);
		} catch(\Exception $e) {
			self::assertTrue(false);
			return;
		}
		//No exception raised
		self::assertTrue(true);
	}

	public function test_withNewTicket_withAdmin_notRaisesException()
	{
		$validator = new TicketValidator();
		try {
			$validator->assertIfUserCanRead($this->ticket, $this->admin);
			$validator->assertIfUserCanModify($this->ticket, $this->admin);
		} catch(\Exception $e) {
			self::assertTrue(false);
			return;
		}
		//No exception raised
		self::assertTrue(true);
	}

	public function test_withTicket_withNotOwnerUser_raisesReadException()
	{
		$this->expectException(ReadNotAllowedException::class);

		$validator = new TicketValidator();
		$validator->assertIfUserCanRead($this->ticket, $this->anotherUser);
		$validator->assertIfUserCanModify($this->ticket, $this->anotherUser);
	}

	public function test_withAssignedTicket_withAssigneeAdmin_notRaisesException()
	{
		$this->ticket = (new TicketBuilder())
			->withOwner($this->defaultUser)
			->withAssignee($this->admin)
			->build();

		$validator = new TicketValidator();
		try {
			$validator->assertIfUserCanRead($this->ticket, $this->admin);
			$validator->assertIfUserCanModify($this->ticket, $this->admin);
		} catch(\Exception $e) {
			self::assertTrue(false);
			return;
		}
		//No exception raised
		self::assertTrue(true);
	}

	public function test_withAssignedTicket_withAnotherAdmin_raisesModifyException()
	{
		$this->expectException(ModifyNotAllowedException::class);

		$this->ticket = (new TicketBuilder())
			->withOwner($this->defaultUser)
			->withAssignee($this->admin)
			->build();

		$validator = new TicketValidator();
		$validator->assertIfUserCanRead($this->ticket, $this->anotherAdmin);
		$validator->assertIfUserCanModify($this->ticket, $this->anotherAdmin);
	}

	public function test_withClosedTicket_withOwner_raisesModifyException()
	{
		$this->expectException(ModifyNotAllowedException::class);

		$this->ticket->close();

		$validator = new TicketValidator();
		$validator->assertIfUserCanRead($this->ticket, $this->defaultUser);
		$validator->assertIfUserCanModify($this->ticket, $this->defaultUser);
	}

	public function test_withClosedTicket_withAssigneeAdmin_raisesModifyException()
	{
		$this->expectException(ModifyNotAllowedException::class);

		$this->ticket = (new TicketBuilder())
			->withOwner($this->defaultUser)
			->withAssignee($this->admin)
			->build();
		$this->ticket->close();

		$validator = new TicketValidator();
		$validator->assertIfUserCanRead($this->ticket, $this->admin);
		$validator->assertIfUserCanModify($this->ticket, $this->admin);
	}
}