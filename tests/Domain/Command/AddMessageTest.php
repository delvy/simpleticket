<?php

namespace App\Tests\Domain\Command;


use App\Domain\Command\AddMessage;
use App\Domain\Command\TicketValidator;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\User\Registered;

class AddMessageTest extends TicketCommandTest
{
	/** @var AddMessage */
	private $addMessageCommand;

	public function setUp()
	{
		parent::setUp();
		$this->addMessageCommand = new AddMessage($this->fakeTicketRepository, new TicketValidator(), $this->fakeDispatcher);
	}


	public function test_whenAddMessage_messageAdded()
	{
		$ticket = Ticket::createTicket(
			'1234',
			$this->userRegistered,
			'testo 1');
		\Phake::when($this->fakeTicketRepository)->findById('1234')->thenReturn($ticket);

		$messageCount = $ticket->messageCount();
		$this->addMessageCommand->execute('1234', 'test del messaggio', $this->userRegistered);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));

		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */
		$this->assertEquals($messageCount+1, $capturedTicket->messageCount());
	}

	public function test_whenAddMessage_withNullBody_itReportError()
	{
		$this->expectException(\Error::class);
		$this->addMessageCommand->execute(1, null, $this->userRegistered);
	}

	public function test_whenAddMessage_withNullUser_itReportError()
	{
		$this->expectException(\Error::class);
		$this->addMessageCommand->execute(1, 'testo', null);
	}

	public function test_whenAddMessage_withNoOwnerUser_itRaisesException()
	{
		$this->expectException(ModifyNotAllowedException::class);

		$anotherUser = \Phake::mock(Registered::class);
		\Phake::when($anotherUser)->getId()->thenReturn('2');

		$this->addMessageCommand->execute('1', 'test del messaggio', $anotherUser);
	}

	public function test_whenAddMessage_withAdminUserForFistReply_thenAssignTicket()
	{
		$this->addMessageCommand->execute('1', 'test del messaggio', $this->userAdminA);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));

		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */
		$this->assertEquals('adminA', $capturedTicket->getAssigneeName());
	}

}