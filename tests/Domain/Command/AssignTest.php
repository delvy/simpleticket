<?php

namespace App\Tests\Domain\Command;


use App\Domain\Command\Assign;
use App\Domain\Command\TicketValidator;
use App\Domain\Ticket\Ticket;

class AssignTest extends TicketCommandTest
{
	/** @var Assign */
	private $assignCommand;

	public function setUp()
	{
		parent::setUp();
		$this->assignCommand = new Assign($this->fakeTicketRepository, new TicketValidator(), $this->fakeDispatcher);
	}


	/**
	 * @expectedException \App\Domain\Exception\ModifyNotAllowedException
	 **/
	public function test_whenAssign_withTicketNotAssigned_itRaisesException()
	{
		$this->assignCommand->execute('1', $this->userAdminA, $this->userAdminB);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));
		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */
		$this->assertEquals('adminB',$capturedTicket->getAssigneeName());
	}

	public function test_whenAssign_itUpdateAssignee()
	{
		$ticket = $this->fakeTicketRepository->findById('1');
		$ticket->assign($this->userAdminA);

		$this->assignCommand->execute('1', $this->userAdminA, $this->userAdminB);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));
		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */
		$this->assertEquals('adminB',$capturedTicket->getAssigneeName());
	}

	public function test_whenAssign_withWrongUserType_itReportError()
	{
		$e = null;
		try {
			$this->assignCommand->execute( '1', $this->userRegistered );
		} catch (\Error $e){}
		$this->assertInstanceOf(\Error::class, $e);
	}

}