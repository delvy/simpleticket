<?php

namespace App\Tests\Domain\Command;

use App\Domain\Command\TicketHandler;
use App\Domain\Event\Dispatcher;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\Admin;
use App\Domain\User\Registered;
use App\Tests\Builder\AdminBuilder;
use App\Tests\Builder\RegisteredBuilder;
use PHPUnit\Framework\TestCase;

abstract class TicketCommandTest extends TestCase
{
	protected $userAdminA;
	protected $userAdminB;
	protected $userRegistered;
	protected $fakeTicketRepository;
	protected $fakeDispatcher;

	public function setUp()
	{
		parent::setUp();
		$this->userAdminA = \Phake::mock(Admin::class);
		\Phake::when($this->userAdminA)->getId()->thenReturn('A');
		\Phake::when($this->userAdminA)->getName()->thenReturn('adminA');
		\Phake::when($this->userAdminA)->isAdmin()->thenReturn(true);

		$this->userAdminB = \Phake::mock(Admin::class);
		\Phake::when($this->userAdminB)->getId()->thenReturn('B');
		\Phake::when($this->userAdminB)->getName()->thenReturn('adminB');
		\Phake::when($this->userAdminB)->isAdmin()->thenReturn(true);

		$this->userRegistered = \Phake::mock(Registered::class);
		\Phake::when($this->userRegistered)->getId()->thenReturn('A');
		\Phake::when($this->userRegistered)->getName()->thenReturn('user');
		\Phake::when($this->userRegistered)->isAdmin()->thenReturn(false);

		$ticket1 = Ticket::createTicket(
			'1',
			$this->userRegistered,
			'testo 1');
		$this->fakeTicketRepository = \Phake::mock(TicketRepository::class);
		\Phake::when($this->fakeTicketRepository)->findById(1)->thenReturn($ticket1);

		$this->fakeDispatcher = \Phake::mock(Dispatcher::class);
	}

}
