<?php

namespace App\Tests\Domain\Command;

use App\Domain\Command\Assign;
use App\Domain\Command\Close;
use App\Domain\Command\TicketValidator;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\User\Registered;

class CloseTest extends TicketCommandTest
{
	/** @var Close */
	private $closeCommand;

	public function setUp()
	{
		parent::setUp();
		$this->closeCommand = new Close($this->fakeTicketRepository, new TicketValidator(), $this->fakeDispatcher);
	}


	public function test_whenClose_itUpdateTicketStatus()
	{
		$this->closeCommand->execute('1', $this->userAdminA);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));
		/** @var Ticket $capturedTicket */
		$this->assertEquals(Ticket::CLOSED,$capturedTicket->getStatus());
	}

	public function test_whenClose_withNoRightUser_itRaisesException()
	{
		$this->expectException(ModifyNotAllowedException::class);

		$anotherUser = \Phake::mock(Registered::class);
		\Phake::when($anotherUser)->getId()->thenReturn('2');

		$this->closeCommand->execute('1', $anotherUser);
	}

	public function test_whenClose_withNullUser_itReportError()
	{
		$this->expectException(\Error::class);
		$this->closeCommand->execute('1', null);
	}

}