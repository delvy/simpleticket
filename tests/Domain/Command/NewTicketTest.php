<?php

namespace App\Tests\Domain\Command;


use App\Domain\Command\NewTicket;
use App\Domain\Command\TicketValidator;
use App\Domain\Ticket\Ticket;

class NewTicketTest extends TicketCommandTest
{
	/** @var NewTicket */
	private $newTicketCommand;

	public function setUp()
	{
		parent::setUp();
		$this->newTicketCommand = new NewTicket($this->fakeTicketRepository, new TicketValidator(), $this->fakeDispatcher);
	}


	public function test_whenNewTicket_ticketCreated()
	{

		$this->newTicketCommand->execute('testo', $this->userRegistered);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));

		$this->assertInstanceOf(Ticket::class, $capturedTicket);
	}

	public function test_whenNewTicket_withNullBody_itReportError()
	{
		$this->expectException(\Error::class);
		$this->newTicketCommand->execute(null, $this->userRegistered);
	}

	public function test_whenNewTicket_withNullUser_itReportError()
	{
		$this->expectException(\Error::class);
		$this->newTicketCommand->execute('testo', null);
	}

	public function test_whenNewTicket_withAdminUser_userIsOwnerAndAssigee()
	{

		$this->newTicketCommand->execute('testo', $this->userAdminA);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));

		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */

		$this->assertEquals('adminA', $capturedTicket->getAssigneeName());
		$this->assertEquals('adminA', $capturedTicket->getOwnerName());
	}

	public function test_whenNewTicket_withRegisteredUser_userIsOwnerAndAssigeeIsNull()
	{

		$this->newTicketCommand->execute('testo', $this->userRegistered);

		\Phake::verify($this->fakeTicketRepository, \Phake::times(1))
		      ->add(\Phake::capture($capturedTicket));

		$this->assertInstanceOf(Ticket::class, $capturedTicket);
		/** @var Ticket $capturedTicket */

		$this->assertEmpty($capturedTicket->getAssigneeName());
		$this->assertEquals('user', $capturedTicket->getOwnerName());
	}

}