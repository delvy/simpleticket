<?php

namespace Tests\Domain\Ticket;

use App\Domain\Ticket\Ticket;
use App\Domain\User\Admin;
use App\Domain\User\Registered;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class TicketTest extends TestCase
{
	private $registered;
	private $admin;

	public function setup()
	{
		$this->registered = \Phake::mock(Registered::class);
		$this->admin = \Phake::mock(Admin::class);
		\Phake::when($this->admin)->toArray()->thenReturn('admin');
	}

	public function test_whenCreate_returnTicketInstance()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		self::assertInstanceOf(Ticket::class,$ticket,'Ticket::create non restituisce un Ticket');
	}

	public function test_whenCreate_acceptAnyUserType_returnTicketInstance()
	{
		$ticket1 = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->admin,
			'testo 1');

		$ticket2 = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		self::assertInstanceOf(Ticket::class,$ticket1,'Ticket::create non restituisce un Ticket');
		self::assertInstanceOf(Ticket::class,$ticket2,'Ticket::create non restituisce un Ticket');
	}

	public function test_whenCreate_setFirstMessage()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		$ticketArray = $ticket->toArray();

		self::assertCount(1, $ticketArray['messages']);
		self::assertEquals('testo 1', $ticketArray['messages'][0]['body']);
	}

	public function test_whenAddMessage_messageWasAdded()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');
		$ticket->addMessage('message', $this->registered);

		$ticketArray = $ticket->toArray();
		self::assertCount(2, $ticketArray['messages']);
		self::assertEquals('message', $ticketArray['messages'][1]['body']);
	}

	public function test_whenAddMoreMessage_sameNumberOfMessageWasAdded()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');
		$ticket->addMessage('message2', $this->registered);
		$ticket->addMessage('message3', $this->registered);
		$ticket->addMessage('message4', $this->registered);
		$ticket->addMessage('message5', $this->registered);

		self::assertEquals(5, $ticket->messageCount());
		$ticketArray = $ticket->toArray();
		self::assertCount(5, $ticketArray['messages']);
	}

	public function test_whenAssign_statusChangeToAssigned()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');
		$ticketArray = $ticket->toArray();
		self::assertEquals(Ticket::NEW, $ticketArray['status']);

		$ticket->assign($this->admin);

		$ticketArray = $ticket->toArray();
		self::assertEquals(Ticket::ASSIGNED, $ticketArray['status']);
	}

	public function test_whenAssign_assigneeChange()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		$ticketArray = $ticket->toArray();
		self::assertNull($ticketArray['assignee']);

		$ticket->assign($this->admin);

		$ticketArray = $ticket->toArray();
		self::assertEquals('admin', $ticketArray['assignee']);
	}

	public function test_whenAssign_updateDate()
	{
		Carbon::setTestNow('2018-11-01');

		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		$ticketArray = $ticket->toArray();
		self::assertEquals('2018-11-01T00:00:00+0000',$ticketArray['updatedAt']);

		Carbon::setTestNow('2018-11-02');
		$ticket->assign($this->admin);

		$ticketArray = $ticket->toArray();
		self::assertEquals('2018-11-02T00:00:00+0000',$ticketArray['updatedAt']);
	}

	public function test_whenClose_statusChange()
	{
		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		$ticketArray = $ticket->toArray();
		self::assertEquals(Ticket::NEW, $ticketArray['status']);

		$ticket->close();

		$ticketArray = $ticket->toArray();
		self::assertEquals(Ticket::CLOSED, $ticketArray['status']);
	}

	public function test_whenClose_updateDate()
	{
		Carbon::setTestNow('2018-11-01');

		$ticket = Ticket::createTicket(
			'12345678-1234-1234-1234-123456789012',
			$this->registered,
			'testo 1');

		$ticketArray = $ticket->toArray();
		self::assertEquals('2018-11-01T00:00:00+0000',$ticketArray['updatedAt']);

		Carbon::setTestNow('2018-11-02');
		$ticket->close();

		$ticketArray = $ticket->toArray();
		self::assertEquals('2018-11-02T00:00:00+0000',$ticketArray['updatedAt']);
	}
}
