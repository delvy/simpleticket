<?php

namespace App\Tests\Infrastructure\Controller;

use App\Domain\Ticket\TicketRepository;
use App\Tests\Infrastructure\DataFixtures\TicketFixture;
use App\Tests\Infrastructure\DataFixtures\UserFixture;
use App\Tests\Infrastructure\TicketBaseFunctionalCase;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class ApiControllerTest extends TicketBaseFunctionalCase
{

	protected function setUp()
	{
		parent::setUp();
		$kernel = self::bootKernel();
		$this->passEncoder = $kernel->getContainer()->get('security.password_encoder');

		$fixturesLoader = new Loader();
		$fixturesLoader->addFixture(new UserFixture($this->passEncoder));
		$fixturesLoader->addFixture(new TicketFixture());

		$purger = new ORMPurger();
		$executor = new ORMExecutor($this->em, $purger);
		$executor->execute($fixturesLoader->getFixtures());
	}

	public function test_homepage_success()
	{
		$userToken = 'utente1token';
		$this->client->request('GET', '/api/', [],[],[
	            'HTTP_X-AUTH-TOKEN' => $userToken
			]);
		$response = $this->client->getResponse()->getContent();
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
	}

	public function test_homepage_responseWithUserId()
	{
		$userToken = 'utente1token';
		$this->client->request('GET', '/api/', [],[],[
			'HTTP_X-AUTH-TOKEN' => $userToken
		]);
		$response = $this->client->getResponse()->getContent();
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertObjectHasAttribute('id',json_decode($response));
	}

	public function test_ticket_withRegisteredUser_responseOnlyUsersTicket()
	{
		$userToken = 'utente1token';

		$this->client->request('GET', '/api/ticket', [],[],[
			'HTTP_X-AUTH-TOKEN' => $userToken
		]);
		$response = $this->client->getResponse()->getContent();

		$user = $this->userRepository->findOneBy(['apiToken'=>$userToken]);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$ticketsArray = json_decode($response, true);
		foreach ($ticketsArray as $ticket) {
			$this->assertEquals($ticket['owner']['id'], $user->getId());
		}
	}

	public function test_detail_withTicketOwnedByUser_responseSuccess()
	{
			$userToken = 'utente1token';
			$ticketId = '11111111-1111-2222-3333-444444444444';

			$this->client->request('GET', '/api/ticket/'.$ticketId, [],[],[
				'HTTP_X-AUTH-TOKEN' => $userToken
			]);
			$response = $this->client->getResponse()->getContent();
			$expectedTicket = $this->ticketRepository->findById($ticketId);
			$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
			$this->assertJson($response);
			$ticket = json_decode($response, true);
			$this->assertEquals($expectedTicket->toArray(), $ticket);
	}

	public function test_detail_withTicketNotOwnedByUser_responseError()
	{
		$userToken = 'utente2token';
		$ticketId = '11111111-1111-2222-3333-444444444444';

		$this->client->request('GET', '/api/ticket/'.$ticketId, [],[],[
			'HTTP_X-AUTH-TOKEN' => $userToken
		]);
		$response = $this->client->getResponse()->getContent();
		$this->assertEquals(400, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(2001,json_decode($response)->error);
	}

	public function test_closeByOwner_withNewTicket_responseSuccess()
	{
		$userToken = 'utente1token';
		$ticketId = '11111111-1111-2222-3333-444444444444';

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/close', [],[],[
			'HTTP_X-AUTH-TOKEN' => $userToken
		]);
		$response = $this->client->getResponse()->getContent();

		$ticket = $this->ticketRepository->findById($ticketId);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(0,json_decode($response)->error);
		$this->assertTrue($ticket->isClose());
	}

	public function test_closeByAdmin_withNewTicket_responseSuccess()
	{
		$userToken = 'admin1token';
		$ticketId = '11111111-1111-2222-3333-444444444444';

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/close', [],[],[
			'HTTP_X-AUTH-TOKEN' => $userToken
		]);
		$response = $this->client->getResponse()->getContent();

		$ticket = $this->ticketRepository->findById($ticketId);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(0,json_decode($response)->error);
		$this->assertTrue($ticket->isClose());
	}

	public function test_assignFromAdminToAnother_responseSuccess()
	{
		$adminToken = 'admin1token';
		$toAdminUserToken = 'admin2token';
		$ticketId = '11111111-1111-2222-3333-444444444444';
		$ticket = $this->ticketRepository->findById($ticketId);
		$admin = $this->userRepository->findOneBy(['apiToken'=>$adminToken]);
		$toAdmin = $this->userRepository->findOneBy(['apiToken'=>$toAdminUserToken]);
		$ticket->assign($admin);

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/assign',
			['toadmin' => $toAdmin->getId()],
			[],
			['HTTP_X-AUTH-TOKEN' => $adminToken ]);
		$response = $this->client->getResponse()->getContent();
		$this->ticketRepository->clear();
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(0,json_decode($response)->error);
		$this->assertTrue($ticket->isAssignee($toAdmin));
	}

	public function test_assignNewTicket_responseError()
	{
		$adminToken = 'admin1token';
		$toAdminUserToken = 'admin2token';
		$ticketId = '22222222-1111-2222-3333-444444444444';
		$toAdmin = $this->userRepository->findOneBy(['apiToken'=>$toAdminUserToken]);

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/assign',
			['toadmin' => $toAdmin->getId()],
			[],
			['HTTP_X-AUTH-TOKEN' => $adminToken ]);
		$response = $this->client->getResponse()->getContent();

		$this->assertEquals(400, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(2001,json_decode($response)->error);
	}

	public function test_autoAssignAlreadyAssignedTicket_responseError()
	{
		$adminToken = 'admin1token';
		$admin2token = 'admin2token';
		$ticketId = '11111111-1111-2222-3333-444444444444';
		$ticket = $this->ticketRepository->findById($ticketId);
		$admin = $this->userRepository->findOneBy(['apiToken'=>$adminToken]);
		$ticket->assign($admin);

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/autoassign',
			[],
			[],
			['HTTP_X-AUTH-TOKEN' => $admin2token ]);
		$response = $this->client->getResponse()->getContent();

		$this->assertEquals(400, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(2001,json_decode($response)->error);
	}

	public function test_autoAssignNewTicket_responseSuccess()
	{
		$adminToken = 'admin1token';
		$ticketId = '22222222-1111-2222-3333-444444444444';
		$toAdmin = $this->userRepository->findOneBy(['apiToken'=>$adminToken]);

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/autoassign',
			[],
			[],
			['HTTP_X-AUTH-TOKEN' => $adminToken ]);
		$response = $this->client->getResponse()->getContent();

		$this->ticketRepository->clear();
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(0,json_decode($response)->error);
		$this->assertTrue($ticket->isAssignee($toAdmin));
	}

	public function test_addMessageByOwner_responseSuccess()
	{
		$registeredToken = 'utente1token';
		$ticketId = '11111111-1111-2222-3333-444444444444';
		$message = 'testo aggiunto dal test funzionale';
		$ticket = $this->ticketRepository->findById($ticketId);
		$messageNumber = count($ticket->getMessages());

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/add-message',
			['message' => $message],
			[],
			['HTTP_X-AUTH-TOKEN' => $registeredToken ]);
		$response = $this->client->getResponse()->getContent();

		$this->ticketRepository->clear();
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(0,json_decode($response)->error);
		$this->assertCount($messageNumber+1, $ticket->getMessages());
	}

	public function test_addMessageByAssignee_responseSuccess()
	{
		$adminToken = 'admin1token';
		$ticketId = '11111111-1111-2222-3333-444444444444';
		$message = 'testo aggiunto dal test funzionale';
		$ticket = $this->ticketRepository->findById($ticketId);
		$messageNumber = count($ticket->getMessages());

		$this->client->request('POST', '/api/ticket/'.$ticketId.'/add-message',
			['message' => $message],
			[],
			['HTTP_X-AUTH-TOKEN' => $adminToken ]);
		$response = $this->client->getResponse()->getContent();

		$this->ticketRepository->clear();
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertEquals(0,json_decode($response)->error);
		$this->assertCount($messageNumber+1, $ticket->getMessages());
	}

	public function test_newTicket_responseSuccess()
	{
		$registeredToken = 'utente1token';
		$message = 'primo messaggio del ticket';
		$user = $this->userRepository->findOneBy(['apiToken'=>$registeredToken]);

		$tickets = $this->ticketRepository->findOwnedByUser($user->getId());
		$ticketsNumber = count($tickets);

		$this->client->request('POST', '/api/new-ticket',
			['message' => $message],
			[],
			['HTTP_X-AUTH-TOKEN' => $registeredToken ]);
		$response = $this->client->getResponse()->getContent();
		$this->ticketRepository->clear();
		$tickets = $this->ticketRepository->findOwnedByUser($user->getId());
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertJson($response);
		$this->assertArrayHasKey('id',json_decode($response, true));
		$this->assertCount($ticketsNumber+1, $tickets);
	}


}
