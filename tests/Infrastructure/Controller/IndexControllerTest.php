<?php

namespace App\Tests\Infrastructure\Controller;

use App\Domain\Ticket\TicketRepository;
use App\Tests\Infrastructure\DataFixtures\TicketFixture;
use App\Tests\Infrastructure\DataFixtures\UserFixture;
use App\Tests\Infrastructure\TicketBaseFunctionalCase;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class IndexControllerTest extends TicketBaseFunctionalCase
{

	protected function setUp()
	{
		parent::setUp();
		$kernel = self::bootKernel();
		$this->passEncoder = $kernel->getContainer()->get('security.password_encoder');

		$fixturesLoader = new Loader();
		$fixturesLoader->addFixture(new UserFixture($this->passEncoder));
		$fixturesLoader->addFixture(new TicketFixture());

		$purger = new ORMPurger();
		$executor = new ORMExecutor($this->em, $purger);
		$executor->execute($fixturesLoader->getFixtures());
	}

	public function test_homepage_redirectToLogin()
	{
		$this->client->request('GET', '/ticketing/');
		$response = $this->client->getResponse()->getContent();
		$this->assertEquals(302, $this->client->getResponse()->getStatusCode());
	}

	public function test_login_redirectToHomepage()
	{
		$crawler = $this->client->request('GET', '/login');

		$form = $crawler->selectButton('Sign in')->form();
		$form['email'] = 'utente1@example.com';
		$form['password'] = 'utentepass';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertTrue($crawler->filter('html:contains("Homepage")')->count() > 0);
	}

	public function test_loginWithBadCredential_redirectToLogin()
	{
		$crawler = $this->client->request('GET', '/login');

		$form = $crawler->selectButton('Sign in')->form();
		$form['email'] = 'utente1@example.com';
		$form['password'] = 'utentepassAAAA';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertTrue($crawler->filter('html:contains("Please sign in")')->count() > 0);
	}

	public function test_newTicketForm()
	{
		$crawler = $this->client->request('GET', '/login');

		$form             = $crawler->selectButton('Sign in')->form();
		$form['email']    = 'utente1@example.com';
		$form['password'] = 'utentepass';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$link = $crawler->filter('a:contains("+ NUOVO TICKET +")')->first()->link();
		$crawler = $this->client->click($link);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertTrue($crawler->filter('html:contains("Nuovo Ticket")')->count() > 0);

		$form = $crawler->selectButton('Save')->form();
		$form['message[body]'] = 'Testo del messaggio';
		$crawler = $this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();
		$this->assertTrue($crawler->filter('html:contains("Testo del messaggio")')->count() > 0);
	}


	public function test_addTicket()
	{
		$crawler = $this->client->request('GET', '/login');

		$form             = $crawler->selectButton('Sign in')->form();
		$form['email']    = 'utente1@example.com';
		$form['password'] = 'utentepass';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$link = $crawler->filter('a:contains("Dettaglio")')->first()->link();
		$crawler = $this->client->click($link);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertTrue($crawler->filter('html:contains("Nuovo Messaggio")')->count() > 0);

		$form = $crawler->selectButton('Save')->form();
		$form['message[body]'] = 'Testo del nuovo messaggio';
		$crawler = $this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();
		$this->assertTrue($crawler->filter('html:contains("Testo del nuovo messaggio")')->count() > 0);
	}

	public function test_closeTicket()
	{
		$crawler = $this->client->request('GET', '/login');

		$form             = $crawler->selectButton('Sign in')->form();
		$form['email']    = 'utente1@example.com';
		$form['password'] = 'utentepass';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$link = $crawler->filter('a:contains("Dettaglio")')->first()->link();
		$crawler = $this->client->click($link);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertTrue($crawler->filter('html:contains("Nuovo Messaggio")')->count() > 0);

		$link = $crawler->filter('a:contains("Chiudi")')->first()->link();
		$crawler = $this->client->click($link);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();
		$this->assertTrue($crawler->filter('html:contains("closed")')->count() > 0);
	}

	public function test_autoassignTicket()
	{
		$crawler = $this->client->request('GET', '/login');

		$form             = $crawler->selectButton('Sign in')->form();
		$form['email']    = 'admin1@example.com';
		$form['password'] = 'adminpass';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$crawler = $this->client->request('GET', '/ticketing/ticket/22222222-1111-2222-3333-444444444444');

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());
		$this->assertTrue($crawler->filter('html:contains("Nuovo Messaggio")')->count() > 0);

		$link = $crawler->filter('a:contains("Prendi in carico")')->first()->link();
		$crawler = $this->client->click($link);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();
		$this->assertTrue($crawler->filter('html:contains("assigned")')->count() > 0);
	}

	public function test_assignTicket()
	{
		$crawler = $this->client->request('GET', '/login');

		$form             = $crawler->selectButton('Sign in')->form();
		$form['email']    = 'admin1@example.com';
		$form['password'] = 'adminpass';
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$crawler = $this->client->request('GET', '/ticketing/ticket/11111111-1111-2222-3333-444444444444/assign');

		$form = $crawler->selectButton('Save')->form();
		$option = $crawler->filter('option:contains("admin2@example.com")');
		$form['admin_list[admin]'] = $option->attr('value');
		$this->client->submit($form);

		$this->assertTrue($this->client->getResponse()->isRedirect());
		$crawler = $this->client->followRedirect();

		$this->assertTrue($crawler->filter('html:contains("assigned")')->count() > 0);
	}
}
