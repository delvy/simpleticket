<?php

namespace App\Tests\Repository;


use App\Domain\Ticket\Ticket;
use App\Infrastructure\Repository\DbTicketRepository;
use App\Tests\Builder\TicketBuilder;
use App\Tests\Infrastructure\DataFixtures\TicketFixture;
use App\Tests\Infrastructure\DataFixtures\UserFixture;
use App\Tests\Infrastructure\TicketBaseFunctionalCase;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DbTicketRepositoryTest extends TicketBaseFunctionalCase
{

	/**
	 * {@inheritDoc}
	 */
	protected function setUp()
	{
		parent::setUp();
		$kernel = self::bootKernel();
		$this->passEncoder = $kernel->getContainer()->get('security.password_encoder');

		$fixturesLoader = new Loader();
		$fixturesLoader->addFixture(new UserFixture($this->passEncoder));
		$fixturesLoader->addFixture(new TicketFixture());

		$purger = new ORMPurger();
		$executor = new ORMExecutor($this->em, $purger);
		$executor->execute($fixturesLoader->getFixtures());
	}

	public function test_findAll()
	{
		$tickets = $this->ticketRepository->findAll();
		$this->assertCount(2, $tickets);
	}

	public function test_add_resultInOneMoreTicket()
	{
		$countPreAdd = count($this->ticketRepository->findAll());
		$user = $this->userRepository->findOneBy(['apiToken' => 'utente1Token']);
		$newTicket = (new TicketBuilder())
			->withOwner($user)
			->build();
		$this->ticketRepository->add($newTicket);

		$this->assertCount($countPreAdd+1, $this->ticketRepository->findAll());
	}

	/**
	 * {@inheritDoc}
	 */
	protected function tearDown()
	{
		parent::tearDown();

	}
}