<?php

namespace App\Tests\Infrastructure;

use App\Domain\Ticket\TicketRepository;
use App\Domain\User\UserRepository;
use App\Tests\Infrastructure\DataFixtures\TicketFixture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;

class TicketBaseFunctionalCase extends WebTestCase
{
	/**
	 * @var Client
	 */
	protected $client;
	/**
	 * @var Router
	 */
	protected $router;
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $em;
	/**
	 * @var TicketRepository
	 */
	protected $ticketRepository;

	/**
	 * @var UserRepository
	 */
	protected $userRepository;

	protected function setUp()
	{
		self::bootKernel();
		$this->ticketRepository = self::$container->get('App\Domain\Ticket\TicketRepository');
		$this->userRepository = self::$container->get('App\Domain\User\UserRepository');

		$this->client = static::createClient();
		$this->router = $this->client->getContainer()->get('router');
		$this->em = $this->client->getContainer()->get('doctrine')->getManager();
		$this->em->getConnection()->query(sprintf('SET FOREIGN_KEY_CHECKS=0'));
	}


}