<?php

namespace App\Tests\Infrastructure\DataFixtures;

use App\Domain\Ticket\Ticket;
use App\Domain\User\Admin;
use App\Domain\User\Registered;
use App\Infrastructure\Repository\DbTicketRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class TicketFixture extends AbstractFixture implements OrderedFixtureInterface
{

	public function load(ObjectManager $manager)
    {
    	/** @var Registered $user1 */
		$user1 = $this->getReference('utente1');
	    /** @var Registered $user2 */
		$user2 = $this->getReference('utente2');
	    /** @var Admin $admin1 */
		$admin1 = $this->getReference('admin1');

		$ticket = Ticket::createTicket('11111111-1111-2222-3333-444444444444', $user1, 'Primo messaggio');
	    $ticket->assign($admin1);
		$ticket->addMessage('Risposta admin', $admin1);
	    $manager->persist($ticket);

	    $ticket = Ticket::createTicket('22222222-1111-2222-3333-444444444444', $user2, 'ticket nuovo');
	    $manager->persist($ticket);

	    $manager->flush();
    }

	public function getOrder()
	{
		return 20;
	}
}
