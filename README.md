# SimpleTicket

##Indroduzione

Simpleticket è una applicazione demo. La finalità di questa applicazione è di mostrare le 
capacità di interpretazione delle specifiche fornite e la qualità di codice e architettura.

Detto questo, si è deciso di concentrarsi esclusivamente su quanto richiesto e tralasciare quanto, seppure 
necessario in un'applicazione reale, non oggetto della valutazione.
Rientrano in questa casistica la gestione degli utenti ed il processo di autenticazione delle API
che oramai possono essere facilmente ottenuti con librerie e bundle molto diffusi e non sembravano utili in questo contesto.

Stesso discorso vale per l'interfaccia grafica e la UX nel quale, nonostante fosse facilmente implementabile
non si tiene conto dei vincoli applicativi (ad esempio i link per compiere determinate azioni sono sempre 
attivi anche se poi le azioni non possono essere eseguite per via dell'applicazione delle logiche di business).

##Installazione
L'applicazione è basata su framework Symfony 4.1 con Flex e file di environment .env.
Quindi è sufficiente, dalla root dell'app eseguire (dopo impostazione del corretto env) un

`composer install`

per installare i vendor, compresi i bundle di Doctrine e quindi eseguire le migrazioni

`bin/console doctrine:migration:migrate`

e poi eseguire le fixture di sviluppo, necessarie per importare gli utenti

`bin/console doctrine:fixture:load`

Questo è sufficiente per avere una applicazione utilizzabile

##Architettura

####Overview
Pur essendo oggettivamente una sovraingegnerizzazione è stato deciso di adottare una
 architettura che separasse nettamente il dominio applicativo dall'infrastruttura.
Si è scelto di utilizzare una architettura esagonale con principi isprirati a Domain-Driven Design e Command Query Separation (senza andare in profondità nell'applicazione di tutti 
i pattern consigliati).

Il principio cardine che si è tentato di seguire è quello di avere un dominio il più possibile pulito da quelle che sono le implementazioni e le dipendenze dal framework.
Ovviamente il tutto è stato ottenuto invertendo le dipendenze ed utilizzando interfacce 
per ogni aspetto che richiede inevitabilemente una implementazione specifica nello strato
di infrastruttura.

##Testing
