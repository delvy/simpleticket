<?php

namespace App\Domain\Subscriber;


use App\Domain\Event\MessageAdded;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MessageAddedOnNewTicket implements EventSubscriberInterface
{

	public static function getSubscribedEvents()
	{
		return ['ticket.messageAdded' => 'execute'];
	}

	private function eventMeetsCondition(MessageAdded $event)
	{
		return $event->getTicket()->isNew();
	}

	public function execute(Event $sfevent)
	{
		$event = $sfevent->getModelEvent();

		if(!$this->eventMeetsCondition($event)){
			return;
		}

		return;
	}
}