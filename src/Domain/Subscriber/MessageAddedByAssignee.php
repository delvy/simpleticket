<?php

namespace App\Domain\Subscriber;

use App\Domain\Event\MessageAdded;
use App\Domain\Notice\Notifier;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MessageAddedByAssignee implements EventSubscriberInterface
{

	public static function getSubscribedEvents()
	{
		return ['ticket.messageAdded' => 'execute'];
	}

	private function eventMeetsCondition(MessageAdded $event)
	{
		return $event->getTicket()->isAssigned() &&
			$event->getTicket()->isAssignee($event->getAuthor());
	}

	public function execute(Event $sfevent)
	{
		/** @var \App\Domain\Event\MessageAdded $event */
		$event = $sfevent->getModelEvent();

		if(!$this->eventMeetsCondition($event)){
			return;
		}
		$ticket = $event->getTicket();
		$owner = $ticket->getOwner(); //mmmm
		Notifier::notify($owner, $ticket);
		return;
	}
}