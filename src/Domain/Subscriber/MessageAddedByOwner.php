<?php

namespace App\Domain\Subscriber;

use App\Domain\Event\MessageAdded;
use App\Domain\Notice\Notifier;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MessageAddedByOwner implements EventSubscriberInterface
{

	public static function getSubscribedEvents()
	{
		return ['ticket.messageAdded' => 'execute'];
	}

	private function eventMeetsCondition(MessageAdded $event)
	{
		return $event->getTicket()->isAssigned() &&
			$event->getTicket()->isOwner($event->getAuthor());
	}

	public function execute(Event $sfevent)
	{
		/** @var \App\Domain\Event\MessageAdded $event */
		$event = $sfevent->getModelEvent();

		if(!$this->eventMeetsCondition($event)){
			return;
		}
		$ticket = $event->getTicket();
		$admin = $ticket->getAssignee();
		Notifier::notify($admin, $ticket);
		return;
	}
}