<?php

namespace App\Domain\Subscriber;


use App\Domain\User\UserRepository;

class SubscriberEnabler
{
	public function enableSubscriber(UserRepository $userRepository)
	{
		return [
			new MessageAddedOnNewTicket(),
			new MessageAddedByOwner(),
			new MessageAddedByAssignee(),
			new TicketCreated($userRepository),
			new TicketNotAssignedClosed($userRepository),
			new TicketClosedByOwner(),
			new TicketClosedByAdmin(),
			new TicketNotAssignedClosed($userRepository)
		];
	}
}