<?php

namespace App\Domain\Subscriber;


use App\Domain\Event\MessageAdded;
use App\Domain\Notice\Notifier;
use App\Domain\Ticket\Ticket;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TicketCreated implements EventSubscriberInterface
{

	private $userRepository;
	/**
	 * MessaggeAddedSubscriber constructor.
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public static function getSubscribedEvents()
	{
		return ['ticket.created' => 'execute'];
	}



	public function execute(Event $sfevent)
	{
		/** @var \App\Domain\Event\MessageAdded $event */
		$event = $sfevent->getModelEvent();
		$ticket = $event->getTicket();
		$admins = $this->userRepository->getAdmins();
		foreach ( $admins as $admin ) {
			Notifier::notify($admin, $ticket);
		}
		return;
	}
}