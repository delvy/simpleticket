<?php

namespace App\Domain\Subscriber;


use App\Domain\Event\MessageAdded;
use App\Domain\Event\TicketClosed;
use App\Domain\Notice\Notifier;
use App\Domain\Ticket\Ticket;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TicketClosedByOwner implements EventSubscriberInterface
{

	public static function getSubscribedEvents()
	{
		return ['ticket.closed' => 'execute'];
	}

	private function eventMeetsCondition(TicketClosed $event)
	{
		return $event->getTicket()->isAssigned() &&
			$event->getTicket()->isOwner($event->getAuthor());
	}

	public function execute(Event $sfevent)
	{
		/** @var \App\Domain\Event\TicketClosed $event */
		$event = $sfevent->getModelEvent();

		if(!$this->eventMeetsCondition($event)){
			return;
		}
		$ticket = $event->getTicket();
		$admin = $ticket->getAssignee();
		Notifier::notify($admin, $ticket);
		return;
	}
}