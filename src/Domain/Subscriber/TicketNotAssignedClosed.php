<?php

namespace App\Domain\Subscriber;

use App\Domain\Event\TicketClosed;
use App\Domain\Notice\Notifier;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TicketNotAssignedClosed implements EventSubscriberInterface
{

	private $userRepository;
	/**
	 * MessaggeAddedSubscriber constructor.
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public static function getSubscribedEvents()
	{
		return ['ticket.closed' => 'execute'];
	}

	private function eventMeetsCondition(TicketClosed $event)
	{
		return !$event->getTicket()->hasAssignee();
	}

	public function execute(Event $sfevent)
	{
		$event = $sfevent->getModelEvent();

		if(!$this->eventMeetsCondition($event)){
			return;
		}
		$ticket = $event->getTicket();
		$admins = $this->userRepository->getAdmins();
		foreach ( $admins as $admin ) {
			Notifier::notify($admin, $ticket);
		}
		return;
	}
}