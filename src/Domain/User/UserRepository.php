<?php

namespace App\Domain\User;


interface UserRepository
{
	public function add(User $user);
	public function findById($id): User;
	public function findAll();
	public function findOneBy(array $criteria, array $orderBy = null);
	public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

	public function getAdmins();
}