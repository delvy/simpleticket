<?php

namespace App\Domain\User;

use App\Domain\Notice\Sender;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class User implements UserInterface
{
    protected $id;
	protected $email;
	protected $roles = [];
	protected $password;
	protected $senderPreferences;
	protected $apiToken;

	/**
	 * User constructor.
	 *
	 * @param $id
	 * @param $email
	 * @param array $roles
	 * @param string $password
	 */
	public function __construct( $id, $email, array $roles)
	{
		$this->id       = $id;
		$this->email    = $email;
		$this->roles    = $roles;
		$this->senderPreferences = new ArrayCollection();
		$this->addSenderPreference(SenderPreference::MAIL_TYPE);
	}

	public function toArray()
	{
		return [
			'id' => $this->id,
			'email' => $this->email
		];
	}

	public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function updateEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $roles = $this->roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function updatePassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
    }

    abstract public function isAdmin();

	public function getName(): string
	{
		return (string) $this->email;
	}

	public function getNotificationList()
	{
		$list = [];
		foreach ( $this->senderPreferences as $senderPreference ) {
			$list[] = $senderPreference->getType();
		}
		return $list;
	}

	public function addSenderPreference($type)
	{
		if (!$this->hasSenderPreference($type)){
			$preference = new SenderPreference(Uuid::uuid4(),$type);
			$preference->setUser($this);
			$this->senderPreferences->add($preference);
		}
	}

	private function hasSenderPreference($type)
	{
		foreach ( $this->senderPreferences as $senderPreference ) {
			if($senderPreference->getType() == $type){
				return true;
			}
		}
		return false;
	}

	public function updateApiToken($token)
	{
		$this->apiToken = $token;
	}
}
