<?php

namespace App\Domain\User;


use Carbon\Carbon;

class SenderPreference
{
	public const MAIL_TYPE = 'mail';
	public const SMS_TYPE = 'sms';
	public const PUSH_TYPE = 'push';

	private $id;
	private $createdAt;
	private $type;
	private $user;

	/**
	 * SenderPreference constructor.
	 *
	 * @param $createdAt
	 * @param $type
	 */
	public function __construct($id, $type )
	{
		$this->id        = $id;
		$this->createdAt = new Carbon();
		$this->type      = $type;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	public function setUser(User $user)
	{
		$this->user = $user;
	}
}