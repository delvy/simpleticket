<?php

namespace App\Domain\User;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Registered extends User
{
	public static function create($id, $email, string $plainPassword, UserPasswordEncoderInterface $encoder)
	{
		$roles = ['ROLE_USER'];
		$user = new Registered(
			$id, $email, $roles
		);
		$user->updatePassword($encoder->encodePassword($user, $plainPassword));
		return $user;
	}

    public function isAdmin()
    {
    	return false;
    }
}
