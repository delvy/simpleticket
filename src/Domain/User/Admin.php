<?php

namespace App\Domain\User;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Admin extends Registered
{
	public static function create( $id, $email, string $plainPassword, UserPasswordEncoderInterface $encoder )
	{
		$roles = ['ROLE_ADMIN'];
		$admin = new Admin(
			$id, $email, $roles
		);
		$admin->updatePassword($encoder->encodePassword($admin, $plainPassword));
		return $admin;
	}

	public function isAdmin()
	{
		return true;
	}

}