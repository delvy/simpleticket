<?php

namespace App\Domain\Command;


use App\Domain\Event\Dispatcher;
use App\Domain\Event\TicketCreated;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\User;
use Ramsey\Uuid\Uuid;

class NewTicket extends TicketCommand
{

	public function __construct(TicketRepository $ticketRepository, TicketValidator $validator,  Dispatcher $dispatcher)
	{
		parent::__construct($ticketRepository, $validator, $dispatcher);
	}

	public function execute($body, User $author)
	{
		$ticket = Ticket::createTicket(Uuid::uuid4(),$author,$body);
		$ticket = $this->tryToAssignTicket($ticket, $author);
		$this->ticketRepository->add($ticket);
		$this->dispatcher->dispatchEvent('ticket.created', new TicketCreated($ticket, $author));
		return $ticket->getId();
	}
}