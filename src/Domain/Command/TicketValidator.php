<?php


namespace App\Domain\Command;

use App\Domain\Event\Dispatcher;
use App\Domain\Event\MessageAdded;
use App\Domain\Event\TicketClosed;
use App\Domain\Event\TicketCreated;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Exception\ReadNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\Admin;
use App\Domain\User\User;
use Ramsey\Uuid\Uuid;


class TicketValidator
{

	/**
	 * @param Ticket $ticket
	 * @param User $user
	 *
	 * @throws ModifyNotAllowedException
	 */
	public function assertIfUserCanRead(Ticket $ticket, User $user)
	{
		if ($user->isAdmin() ||
		    $ticket->isOwner($user)){
			return;
		}
		throw new ReadNotAllowedException('Read not allowed');
	}

	/**
	 * @param Ticket $ticket
	 * @param User $user
	 *
	 * @throws ModifyNotAllowedException
	 */
	public function assertIfUserCanModify(Ticket $ticket, User $user)
	{
		if (
			(($user->isAdmin() && $ticket->isNew())
		        || $ticket->isAssignee($user)
		        || $ticket->isOwner($user))
			&& !$ticket->isClose()){
			return;
		}
		throw new ModifyNotAllowedException('Modify not allowed');
	}

}