<?php

namespace App\Domain\Command;


use App\Domain\Event\Dispatcher;
use App\Domain\Event\MessageAdded;
use App\Domain\Event\TicketCreated;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\User;
use Ramsey\Uuid\Uuid;

class AddMessage extends TicketCommand
{

	public function __construct(TicketRepository $ticketRepository, TicketValidator $validator,  Dispatcher $dispatcher)
	{
		parent::__construct($ticketRepository, $validator, $dispatcher);
	}

	/**
	 * @param $ticketId
	 * @param $body
	 * @param User $author
	 *
	 * @throws ModifyNotAllowedException
	 */
	public function execute(string $ticketId, string $body, User $author)
	{
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->validator->assertIfUserCanModify($ticket, $author);
		$ticket->addMessage($body, $author);
		$ticket = $this->tryToAssignTicket($ticket, $author);
		$this->ticketRepository->add($ticket);
		$this->dispatcher->dispatchEvent('ticket.messageAdded', new MessageAdded($ticket, $author));
	}
}