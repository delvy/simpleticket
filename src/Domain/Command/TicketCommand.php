<?php


namespace App\Domain\Command;

use App\Domain\Event\Dispatcher;
use App\Domain\Event\MessageAdded;
use App\Domain\Event\TicketClosed;
use App\Domain\Event\TicketCreated;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\Admin;
use App\Domain\User\User;
use Ramsey\Uuid\Uuid;


abstract class TicketCommand
{
	protected $ticketRepository;
	protected $validator;
	protected $dispatcher;

	/**
	 * TicketHandler constructor.
	 *
	 * @param TicketRepository $ticketRepository
	 * @param TicketValidator $validator
	 * @param Dispatcher $dispatcher
	 */
	public function __construct(TicketRepository $ticketRepository, TicketValidator $validator, Dispatcher $dispatcher )
	{
		$this->ticketRepository = $ticketRepository;
		$this->validator = $validator;
		$this->dispatcher = $dispatcher;
	}


	protected function tryToAssignTicket( Ticket $ticket, User $user )
	{
		if($user->isAdmin() && $ticket->isNew()){
			$ticket->assign($user);
		}
		return $ticket;
	}

}