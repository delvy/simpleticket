<?php

namespace App\Domain\Command;

use App\Domain\Event\Dispatcher;
use App\Domain\Event\TicketClosed;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\Admin;
use App\Domain\User\User;

class Close extends TicketCommand
{

	public function __construct(TicketRepository $ticketRepository, TicketValidator $validator,  Dispatcher $dispatcher)
	{
		parent::__construct($ticketRepository, $validator, $dispatcher);
	}

	/**
	 * @param $ticketId
	 * @param User $user
	 *
	 * @throws ModifyNotAllowedException
	 */
	public function execute($ticketId, User $user)
	{
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->validator->assertIfUserCanModify($ticket, $user);
		$ticket->close();
		$this->ticketRepository->add($ticket);
		$this->dispatcher->dispatchEvent('ticket.closed', new TicketClosed($ticket, $user));
	}

}