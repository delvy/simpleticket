<?php

namespace App\Domain\Command;

use App\Domain\Event\Dispatcher;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\Admin;

class Assign extends TicketCommand
{

	public function __construct(TicketRepository $ticketRepository, TicketValidator $validator,  Dispatcher $dispatcher)
	{
		parent::__construct($ticketRepository, $validator, $dispatcher);
	}

	/**
	 * @param $ticketId
	 * @param Admin $fromAdmin
	 * @param Admin $toAdmin
	 *
	 * @throws ModifyNotAllowedException
	 */
	public function execute($ticketId, Admin $fromAdmin, Admin $toAdmin)
	{
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->checkIfUserIsCurrentAssignee($ticket, $fromAdmin);
		$ticket->assign( $toAdmin );
		$this->ticketRepository->add( $ticket );
	}

	/**
	 * @param Ticket $ticket
	 * @param Admin $user
	 *
	 * @throws ModifyNotAllowedException
	 */
	private function checkIfUserIsCurrentAssignee(Ticket $ticket,Admin $user)
	{
		if($ticket->isAssignee($user)) {
			return;
		}
		throw new ModifyNotAllowedException('Modify not allowed');
	}
}