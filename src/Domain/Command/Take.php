<?php

namespace App\Domain\Command;


use App\Domain\Event\Dispatcher;
use App\Domain\Event\MessageAdded;
use App\Domain\Event\TicketCreated;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use App\Domain\User\Admin;
use App\Domain\User\User;
use Ramsey\Uuid\Uuid;

class Take extends TicketCommand
{

	public function __construct(TicketRepository $ticketRepository, TicketValidator $validator,  Dispatcher $dispatcher)
	{
		parent::__construct($ticketRepository, $validator, $dispatcher);
	}

	/**
	 * @param $ticketId
	 * @param Admin $toAdmin
	 *
	 * @throws ModifyNotAllowedException
	 */
	public function execute($ticketId, Admin $toAdmin)
	{
		$ticket = $this->ticketRepository->findById($ticketId);
		$this->checkIfTicketIsNew($ticket);
		$ticket->assign($toAdmin);
		$this->ticketRepository->add($ticket);
	}

	/**
	 * @param Ticket $ticket
	 *
	 * @throws ModifyNotAllowedException
	 */
	private function checkIfTicketIsNew(Ticket $ticket)
	{
		if($ticket->isNew()) {
			return;
		}
		throw new ModifyNotAllowedException('Modify not allowed');
	}
}