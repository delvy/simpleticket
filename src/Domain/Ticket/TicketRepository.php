<?php

namespace App\Domain\Ticket;


interface TicketRepository
{
	public function add(Ticket $ticket);
	public function findById($id): Ticket;
	public function findAll();
	public function findOwnedByUser($id);
}