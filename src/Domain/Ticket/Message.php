<?php

namespace App\Domain\Ticket;


use App\Domain\User\User;
use Carbon\Carbon;

class Message
{
	private $id;
	private $createdAt;
	private $author;
	private $body;
	private $number;
	private $ticket;

	/**
	 * Message constructor.
	 *
	 * @param $id
	 * @param $author
	 * @param $body
	 * @param $number
	 */
	public function __construct(string $id, User $author, string $body, int $number )
	{
		$this->id     = $id;
		$this->author = $author;
		$this->body   = $body;
		$this->number = $number;
		$this->createdAt = new Carbon();
	}

	public function toArray()
	{
		return [
			'id' => $this->id,
			'author' => $this->author->toArray(),
			'body' => $this->body,
			'number' => $this->number,
			'createdAt' => $this->createdAt->format(Carbon::ISO8601)
		];
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return Carbon
	 */
	public function getCreationDate(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @return mixed
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * @return mixed
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param mixed $ticket
	 */
	public function updateTicket( $ticket ): void
	{
		$this->ticket = $ticket;
	}

	public function isFirst(){
		return $this->number == 1;
	}

	public function getAuthorName(): string
	{
		return $this->author->getName();
	}
}