<?php

namespace App\Domain\Ticket;


use App\Domain\User\Admin;
use App\Domain\User\User;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;

class Ticket
{
	public const NEW = 'new';
	public const ASSIGNED = 'assigned';
	public const CLOSED = 'closed';

	private $id;
	/** @var Carbon */
	private $createdAt;
	private $updatedAt;
	private $status;
	private $owner;
	private $assignee;
	private $messages;

	/**
	 * Ticket constructor.
	 *
	 * @param $id
	 * @param $createdAt
	 * @param $status
	 * @param $owner
	 */
	public function __construct( $id, $createdAt, $status, User $owner )
	{
		$this->id        = $id;
		$this->createdAt = $createdAt;
		$this->status    = $status;
		$this->owner     = $owner;
		$this->messages  = new ArrayCollection();
	}

	public function toArray()
	{
		return [
			'id' => $this->id,
			'createdAt' => $this->createdAt->format(Carbon::ISO8601),
			'updatedAt' => $this->updatedAt->format(Carbon::ISO8601),
			'status' => $this->status,
			'owner' =>  $this->owner->toArray(),
			'assignee' => $this->assignee ? $this->assignee->toArray() : null,
			'messages' => $this->messagesToArray()
		];
	}

	private function messagesToArray()
	{
		$return = [];
		foreach ( $this->getOrderedMessages() as $message ) {
			$return[] = $message->toArray();
		}
		return $return;
	}

	public static function createTicket( $id, User $owner, $text): Ticket
	{
		$ticket = new Ticket($id,new Carbon(), self::NEW, $owner);
		$ticket->addMessage($text, $owner);
		return $ticket;
	}

	public function getId()
	{
		return $this->id;
	}

	public function addMessage(string $text, User $author): void
	{
		$message = new Message(
			Uuid::uuid4(),
			$author,
			$text,
			$this->nextMessageNumber()
		);
		$message->updateTicket($this);
		$this->messages->add($message);
		$this->update();
	}


	public function messageCount(): int
	{
		$max = 0;
		foreach($this->messages as $message){
			if($message->getNumber() > $max){
				$max = $message->getNumber();
			}
		}
		return $max;
	}

	private function nextMessageNumber(): int
	{
		$max = $this->messageCount();
		$max++;
		return $max;
	}

	public function assign(Admin $assignee): void
	{
		$this->status = self::ASSIGNED;
		$this->assignee = $assignee;
		$this->update();
	}

	public function close(): void
	{
		$this->status = self::CLOSED;
		$this->update();
	}

	private function update(): void
	{
		$this->updatedAt = new Carbon();
	}

	public function getCreationDate(): \Datetime
	{
		return $this->createdAt;
	}

	public function getStatus(): string
	{
		return $this->status;
	}

	public function getTitle(): string
	{
		$message = $this->firstMessage();
		return $message->getBody();
	}

	private function firstMessage(): Message
	{
		foreach($this->messages as $message){
			if($message->isFirst()){
				return $message;
			}
		}
	}

	public function getOwnerName(): string
	{
		return $this->owner->getName();
	}

	public function getAssigneeName(): string
	{
		if($this->hasAssignee()) {
			return $this->assignee->getName();
		}
		return '';
	}

	public function hasAssignee()
	{
		if(!empty($this->assignee)) {
			return true;
		}
		return false;
	}

	public function isAssignee(User $user)
	{
		if($this->hasAssignee()) {
			return $user->getId() == $this->assignee->getId();
		}
		return false;
	}

	public function isOwner(User $user)
	{
		return $user->getId() == $this->owner->getId();
	}

	public function isNew()
	{
		return $this->status == self::NEW;
	}

	public function isAssigned()
	{
		return $this->status == self::ASSIGNED;
	}

	public function isClose()
	{
		return $this->status == self::CLOSED;
	}

	public function getOrderedMessages()
	{
		$iterator = $this->messages->getIterator();
		$iterator->uasort(function (Message $a, Message $b) {
			return ($a->getNumber() < $b->getNumber()) ? -1 : 1;
		});
		$collection = new ArrayCollection(iterator_to_array($iterator));
		return $collection;
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function getAssignee(): Admin
	{
		if($this->hasAssignee()){
			return $this->assignee;
		}
	}

	public function getOwner(): Admin
	{
		return $this->assignee;
	}
}