<?php

namespace App\Domain\Event;


use App\Domain\Ticket\Ticket;
use App\Domain\User\User;

class TicketClosed extends Event
{
	private $ticket;
	private $user;

	/**
	 * MessageAdded constructor.
	 *
	 * @param $ticket
	 * @param $author
	 */
	public function __construct(Ticket $ticket,User $user )
	{
		$this->ticket = $ticket;
		$this->user = $user;
	}

	/**
	 * @return Ticket
	 */
	public function getTicket(): Ticket
	{
		return $this->ticket;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

}