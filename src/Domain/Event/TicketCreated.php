<?php

namespace App\Domain\Event;


use App\Domain\Ticket\Ticket;
use App\Domain\User\User;

class TicketCreated extends Event
{
	private $ticket;
	private $author;

	/**
	 * MessageAdded constructor.
	 *
	 * @param $ticket
	 * @param $author
	 */
	public function __construct(Ticket $ticket,User $author )
	{
		$this->ticket = $ticket;
		$this->author = $author;
	}

	/**
	 * @return Ticket
	 */
	public function getTicket(): Ticket
	{
		return $this->ticket;
	}

	/**
	 * @return User
	 */
	public function getAuthor(): User
	{
		return $this->author;
	}

}