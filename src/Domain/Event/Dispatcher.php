<?php

namespace App\Domain\Event;


interface Dispatcher
{
	public function dispatchEvent($name, Event $event);
}