<?php

namespace App\Domain\Notice;


use App\Domain\Ticket\Ticket;
use App\Domain\User\User;

class Notifier
{
	public static function notify(User $user, Ticket $ticket)
	{
		$senderTypeSelected = $user->getNotificationList();
		foreach($senderTypeSelected as $senderType){
			$sender = SenderFactory::create($senderType);
			$sender->send($ticket);
		}
	}
}