<?php

namespace App\Domain\Notice;


use App\Domain\Ticket\Ticket;

class NullSender implements Sender
{

	public function send(Ticket $ticket)
	{
		//null
	}
}