<?php

namespace App\Domain\Notice;


use App\Domain\Ticket\Ticket;

interface Sender
{
	public function send(Ticket $ticket);
}