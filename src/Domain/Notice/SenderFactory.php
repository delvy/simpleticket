<?php

namespace App\Domain\Notice;


use App\Domain\User\SenderPreference;

class SenderFactory
{


	public static function create($type)
	{
		switch ($type) {
			case SenderPreference::MAIL_TYPE :
				return new MailSender();
			case SenderPreference::SMS_TYPE :
				return new SmsSender();
			case SenderPreference::PUSH_TYPE :
				return new PushSender();
			default :
				return new NullSender();
		};
	}
}