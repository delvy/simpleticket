<?php

namespace App\Infrastructure\Repository;

use App\Domain\User\Registered;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class DbUserRepository implements UserRepository
{
	/**
	 * @var EntityRepository
	 */
	private $doctrineRepository;
	/**
	 * @var EntityManagerInterface
	 */
	private $em;

    public function __construct(EntityManagerInterface $em)
    {
	    $this->em = $em;
	    $this->doctrineRepository = $this->em->getRepository(Registered::class);
    }

	public function add(User $user)
	{
		$this->em->persist($user);
		$this->em->flush();
	}

	public function findAll()
	{
		$users = $this->doctrineRepository->findAll();
		return $users;
	}

	/**
	 * @param $id
	 *
	 * @return User
	 */
	public function findById($id): User
	{
		$user = $this->doctrineRepository->find($id);
		return $user;
	}

	public function findOneBy( array $criteria, array $orderBy = null )
	{
		$user = $this->doctrineRepository->findOneBy($criteria, $orderBy);
		return $user;
	}

	public function findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
	{
		$users = $this->doctrineRepository->findBy($criteria, $orderBy, $limit, $offset);
		return $users;
	}

	public function getAdmins()
	{
		$qb = $this->doctrineRepository->createQueryBuilder('u');
		$qb->where('u INSTANCE OF :discr');
		$qb->setParameter('discr', 'admin');
		return $qb->getQuery()->getResult();
	}
}
