<?php

namespace App\Infrastructure\Repository;

use App\Domain\Exception\TicketNotFoundException;
use App\Domain\Ticket\Ticket;
use App\Domain\Ticket\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class DbTicketRepository implements TicketRepository
{
	/**
	 * @var EntityRepository
	 */
	private $doctrineRepository;
	/**
	 * @var EntityManagerInterface
	 */
	private $em;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->doctrineRepository = $this->em->getRepository(Ticket::class);

	}

	public function add(Ticket $ticket)
	{
		$this->em->persist($ticket);
		$this->em->flush();
	}

	public function findAll()
	{
		$tickets = $this->doctrineRepository->findAll();
		return $tickets;
	}

	/**
	 * @param $id
	 *
	 * @return Ticket
	 * @throws TicketNotFoundException
	 */
	public function findById($id): Ticket
	{
		$ticket = $this->doctrineRepository->find($id);
		if (empty($ticket)) {
			throw new TicketNotFoundException( 'Ticket not found' );
		}
		return $ticket;
	}

	public function findOwnedByUser( $id )
	{
		$tickets = $this->doctrineRepository->findBy(['owner' => $id]);
		return $tickets;
	}

	public function clear()
	{
		$this->doctrineRepository->clear();
	}
}