<?php

namespace App\Infrastructure\Event;


use Symfony\Component\EventDispatcher\Event;

class SfEvent extends Event
{
	private $modelEvent;

	/**
	 * SfEvent constructor.
	 *
	 * @param $modelEvent
	 */
	public function __construct(\App\Domain\Event\Event $modelEvent )
	{
		$this->modelEvent = $modelEvent;
	}

	/**
	 * @return \App\Domain\Event\Event
	 */
	public function getModelEvent(): \App\Domain\Event\Event
	{
		return $this->modelEvent;
	}


}