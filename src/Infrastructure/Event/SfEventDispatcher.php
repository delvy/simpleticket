<?php

namespace App\Infrastructure\Event;


use App\Domain\Event\Dispatcher;
use App\Domain\Event\Event;
use App\Domain\Subscriber\SubscriberEnabler;
use App\Infrastructure\Repository\DbUserRepository;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SfEventDispatcher extends EventDispatcher implements Dispatcher
{

	public function __construct(DbUserRepository $userRepository)
	{
		$enabler = new SubscriberEnabler();
		foreach($enabler->enableSubscriber($userRepository) as $subscriber) {
			$this->addSubscriber( $subscriber );
		}
	}

	public function dispatchEvent( $name, Event $event )
	{
		$event = new SfEvent($event);
		$this->dispatch($name, $event);
	}

}