<?php

namespace App\Infrastructure\Controller;

use App\Domain\Command\AddMessage;
use App\Domain\Command\Assign;
use App\Domain\Command\Close;
use App\Domain\Command\NewTicket;
use App\Domain\Command\Take;
use App\Domain\Command\TicketValidator;
use App\Infrastructure\Form\AdminListType;
use App\Infrastructure\Form\MessageType;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Exception\TicketingException;
use App\Infrastructure\Repository\DbTicketRepository;
use App\Infrastructure\Repository\DbUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

/**
 * Class IndexController
 * @package App\Controller
 */
class IndexController extends AbstractController
{

	/**
	 * @Route("/", name="index")
	 * @Route("/ticketing/", name="homepage")
	 * @param DbTicketRepository $ticketRepository
	 *
	 * @return Response
	 */
	public function homepage(DbTicketRepository $ticketRepository): Response
	{
		if ($this->getUser()->isAdmin()){
			$tickets = $ticketRepository->findAll();
		} else {
			$tickets = $ticketRepository->findOwnedByUser($this->getUser()->getId());
		}

		return $this->render('app/homepage.html.twig', [
			'tickets' => $tickets
		]);
	}

	/**
	 * @Route("/ticketing/new", name="ticket-new", methods={"GET","POST"})
	 * @param NewTicket $command
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function new(NewTicket $command, Request $request): Response
	{
		try {

			$form = $this->createForm(MessageType::class);

			$form->handleRequest($request);
			if ($form->isSubmitted() && $form->isValid()){
				$message = $form->getData();
				$command->execute($message->getBody(), $this->getUser());
				$this->addFlash('success', 'Ticket aggiunto con successo');

				return $this->redirectToRoute('homepage');
			}
		} catch (TicketingException $e) {
			return $this->genericErrorResponse();
		}

		return $this->render('app/ticket-new.html.twig', [
			'form' => $form->createView()
		]);
	}


	/**
	 * @Route("/ticketing/ticket/{ticketId}", name="ticket", methods={"GET","POST"})
	 * @param DbTicketRepository $ticketRepository
	 * @param AddMessage $ticketCommand
	 * @param TicketValidator $validator
	 * @param Request $request
	 * @param $ticketId
	 *
	 * @return Response
	 */
	public function detail(DbTicketRepository $ticketRepository, AddMessage $ticketCommand, TicketValidator $validator, Request $request, $ticketId): Response
	{
		try {
			$ticket = $ticketRepository->findById($ticketId);
			$validator->assertIfUserCanRead($ticket, $this->getUser());

			$form = $this->createForm(MessageType::class);

			$form->handleRequest($request);
			if ($form->isSubmitted() && $form->isValid()){
				$message = $form->getData();
				$ticketCommand->execute($ticketId, $message->getBody(), $this->getUser());
				$this->addFlash('success', 'Ticket aggiornato con successo');

				return $this->redirect($request->getUri());
			}
		} catch (ModifyNotAllowedException $e) {
			$this->addFlash('danger', 'Non puoi modificare questo ticket');
		} catch (TicketingException $e) {
			return $this->genericErrorResponse();
		}

		return $this->render('app/ticket.html.twig', [
			'ticket' => $ticket,
			'form'   => $form->createView()
		]);
	}

	/**
	 * @Route("/ticketing/ticket/{ticketId}/assign", name="ticket-assign", methods={"GET","POST"})
	 * @param DbUserRepository $userRepository
	 * @param DbTicketRepository $ticketRepository
	 * @param Assign $ticketCommand
	 * @param TicketValidator $validator
	 * @param Request $request
	 * @param $ticketId
	 *
	 * @return Response
	 */
	public function assign(DbUserRepository $userRepository, DbTicketRepository $ticketRepository, Assign $ticketCommand, TicketValidator $validator, Request $request, $ticketId): Response
	{
		try {
			$ticket = $ticketRepository->findById($ticketId);

			$admins = $userRepository->getAdmins();
			$form   = $this->createForm(AdminListType::class, null, [
				'admins' => $admins
			]);

			$validator->assertIfUserCanModify($ticket, $this->getUser());

			$form->handleRequest($request);
			if ($form->isSubmitted() && $form->isValid()){
				$assigneeAdmin = $form->getData();
				$toAdmin       = $userRepository->findById($assigneeAdmin['admin']);
				$ticketCommand->execute($ticketId, $this->getUser(), $toAdmin);
				$this->addFlash('success', 'Ticket aggiornato con successo');

				return $this->redirectToRoute('ticket', [ 'ticketId' => $ticketId ]);
			}
		} catch (ModifyNotAllowedException $e) {
			$this->addFlash('danger', 'Non puoi modificare questo ticket');
		} catch (TicketingException $e) {
			return $this->genericErrorResponse();
		}

		return $this->render('app/ticket-assign.html.twig', [
			'ticket' => $ticket,
			'form'   => $form->createView()
		]);
	}

	/**
	 * @Route("/ticketing/ticket/{ticketId}/autoassign", name="ticket-autoassign", methods={"GET","POST"})
	 * @param DbTicketRepository $ticketRepository
	 * @param Take $ticketCommand
	 * @param TicketValidator $validator
	 * @param $ticketId
	 *
	 * @return Response
	 */
	public function autoassign(DbTicketRepository $ticketRepository, Take $ticketCommand, TicketValidator $validator, $ticketId): Response
	{
		try {
			$ticket = $ticketRepository->findById($ticketId);
			$validator->assertIfUserCanModify($ticket, $this->getUser());
			$ticketCommand->execute($ticketId, $this->getUser());
			$this->addFlash('success', 'Ticket aggiornato con successo');
		} catch (TypeError $e) {
			$this->addFlash('danger', 'Non sei autorizzato a prendere in carico il ticket');
		} catch (ModifyNotAllowedException $e) {
			$this->addFlash('danger', 'Non puoi modificare questo ticket');
		} catch (TicketingException $e) {
			return $this->genericErrorResponse();
		}

		return $this->redirectToRoute('ticket', [ 'ticketId' => $ticketId ]);
	}

	/**
	 * @Route("/ticketing/ticket/{ticketId}/close", name="ticket-close", methods={"GET","POST"})
	 * @param DbTicketRepository $ticketRepository
	 * @param Close $ticketCommand
	 * @param TicketValidator $validator
	 * @param $ticketId
	 *
	 * @return Response
	 */
	public function close(DbTicketRepository $ticketRepository, Close $ticketCommand, TicketValidator $validator, $ticketId): Response
	{
		try {
			$ticket = $ticketRepository->findById($ticketId);
			$validator->assertIfUserCanModify($ticket, $this->getUser());
			$ticketCommand->execute($ticketId, $this->getUser());
			$this->addFlash('success', 'Ticket aggiornato con successo');
		} catch (ModifyNotAllowedException $e) {
			$this->addFlash('danger', 'Non puoi modificare questo ticket');
		} catch (TicketingException $e) {
			return $this->genericErrorResponse();
		}

		return $this->redirectToRoute('ticket', [ 'ticketId' => $ticketId ]);
	}

	private function genericErrorResponse()
	{
		return $this->render('error.html.twig');
	}
}