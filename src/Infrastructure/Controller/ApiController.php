<?php

namespace App\Infrastructure\Controller;

use App\Domain\Command\AddMessage;
use App\Domain\Command\Assign;
use App\Domain\Command\Close;
use App\Domain\Command\NewTicket;
use App\Domain\Command\Take;
use App\Domain\Command\TicketValidator;
use App\Domain\Exception\ModifyNotAllowedException;
use App\Domain\Exception\TicketingException;
use App\Infrastructure\Repository\DbTicketRepository;
use App\Infrastructure\Repository\DbUserRepository;
use Assert\LazyAssertionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TypeError;

/**
 * Class IndexController
 * @package App\Controller
 * @Route("/api")
 */
class ApiController extends AbstractController
{

	public CONST INPUT_DATA_VALIDATION_ERROR = 1001;
	public CONST DOMAIN_FAILURE = 2001;

	/**
	 * @Route("/", name="api-index")
	 */
	public function homepage(): Response
	{
		$user = $this->getUser();
		return new JsonResponse($user->toArray());
	}

	/**
	 * @Route("/ticket", name="api-ticket-list")
	 */
	public function ticket(DbTicketRepository $ticketRepository): Response
	{
		if($this->getUser()->isAdmin()){
			$tickets = $ticketRepository->findAll();
		} else {
			$tickets = $ticketRepository->findOwnedByUser( $this->getUser()->getId() );
		}
		$response = [];
		foreach ( $tickets as $ticket ) {
			$response[] = $ticket->toArray();
		}
		return new JsonResponse($response);
	}

	/**
	 * @Route("/new-ticket", name="api-new-ticket", methods={"POST"})
	 */
	public function new(NewTicket $command, Request $request): Response
	{

		$assertions = \Assert\lazy()
			->that($request->get('message'), 'message', 'message must be a string. %s provided.')->string();
		try {
			$assertions->verifyNow();
		} catch (LazyAssertionException $e) {
			return $this->errorResponse($e, self::INPUT_DATA_VALIDATION_ERROR);
		}
		try {
			$ticketId = $command->execute($request->get('message'), $this->getUser());
		} catch (TicketingException $e){
			return $this->errorResponse($e, self::DOMAIN_FAILURE);
		}

		return new JsonResponse(['id' => $ticketId]);
	}


	/**
	 * @Route("/ticket/{ticketId}", name="api-ticket-single", methods={"GET"})
	 */
	public function detail(DbTicketRepository $ticketRepository, TicketValidator $validator, $ticketId): Response
	{
		try {
			$ticket = $ticketRepository->findById( $ticketId );
			$validator->assertIfUserCanRead($ticket, $this->getUser());
			return new JsonResponse($ticket->toArray());
		} catch (TicketingException $e){
			return $this->errorResponse($e, self::DOMAIN_FAILURE);
		}
	}

	/**
	 * @Route("/ticket/{ticketId}/add-message", name="api-ticket-add-message", methods={"POST"})
	 */
	public function addMessage(AddMessage $command, Request $request, $ticketId): Response
	{
		$assertions = \Assert\lazy()
			->that($ticketId, 'ticketId', 'ticketId must be a uuid. %s provided.')->uuid()
			->that($request->get('message'), 'message', 'message must be a string. %s provided.')->string();
		try {
			$assertions->verifyNow();
		} catch (LazyAssertionException $e) {
			return $this->errorResponse($e, self::INPUT_DATA_VALIDATION_ERROR);
		}

		try {
			$command->execute($ticketId, $request->get('message'), $this->getUser());
		} catch (TicketingException $e){
			return $this->errorResponse($e, self::DOMAIN_FAILURE);
		}

		return $this->successResponse('Message added');
	}


	/**
	 * @Route("/ticket/{ticketId}/assign", name="api-ticket-assign", methods={"POST"})
	 */
	public function assign(DbUserRepository $userRepository, Assign $command, Request $request, $ticketId): Response
	{
		$assertions = \Assert\lazy()
			->that($ticketId, 'ticketId', 'ticketId must be a uuid. %s provided.')->uuid()
			->that($request->get('toadmin'), 'toadmin', 'toadmin must be a uuid. %s provided.')->uuid();
		try {
			$assertions->verifyNow();
		} catch (LazyAssertionException $e) {
			return $this->errorResponse($e, self::INPUT_DATA_VALIDATION_ERROR);
		}
		
		try {
			$toAdmin = $userRepository->findById($request->get('toadmin'));
			$command->execute($ticketId, $this->getUser(), $toAdmin);
		} catch (TicketingException $e){
			return $this->errorResponse($e, self::DOMAIN_FAILURE);
		}

		return $this->successResponse('Ticket assigned');
	}

	/**
	 * @Route("/ticket/{ticketId}/autoassign", name="api-ticket-autoassign", methods={"POST"})
	 */
	public function autoassign(Take $command, $ticketId): Response
	{
		$assertions = \Assert\lazy()
			->that($ticketId, 'ticketId', 'ticketId must be a uuid. %s provided.')->uuid();
		try {
			$assertions->verifyNow();
		} catch (LazyAssertionException $e) {
			return $this->errorResponse($e, self::INPUT_DATA_VALIDATION_ERROR);
		}

		try {
			$command->execute($ticketId, $this->getUser());
		} catch (TypeError $e) {
			throw new ModifyNotAllowedException();
		} catch (TicketingException $e){
			return $this->errorResponse($e, self::DOMAIN_FAILURE);
		}

		return $this->successResponse('Ticket assigned');
	}

	/**
	 * @Route("/ticket/{ticketId}/close", name="api-ticket-close", methods={"POST"})
	 */
	public function close(Close $command, $ticketId): Response
	{
		$assertions = \Assert\lazy()
			->that($ticketId, 'ticketId', 'ticketId must be a uuid. %s provided.')->uuid();
		try {
			$assertions->verifyNow();
		} catch (LazyAssertionException $e) {
			return $this->errorResponse($e, self::INPUT_DATA_VALIDATION_ERROR);
		}

		try {
			$command->execute($ticketId, $this->getUser());
		} catch (TicketingException $e){
			return $this->errorResponse($e, self::DOMAIN_FAILURE);
		}

		return $this->successResponse('Ticket closed');
	}

	private function errorResponse(\Exception $e, $code = 0)
	{
		$response = [
			'error' => $code,
			'message' => $e->getMessage()
		];
		return new JsonResponse($response, 400);
	}

	private function successResponse($message)
	{
		$response = [
			'error' => 0,
			'message' => $message
		];
		return new JsonResponse($response, 200);
	}
}