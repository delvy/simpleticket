<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Ticket\Ticket;
use App\Domain\User\Admin;
use App\Domain\User\Registered;
use App\Infrastructure\Repository\DbTicketRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class TicketFixture extends Fixture implements OrderedFixtureInterface
{
	private $ticketRepository;

	public function __construct(DbTicketRepository $ticketRepository)
	{
		$this->ticketRepository = $ticketRepository;
	}

	public function load(ObjectManager $manager)
    {
    	/** @var Registered $user1 */
		$user1 = $this->getReference('utente1');
	    /** @var Registered $user2 */
		$user2 = $this->getReference('utente2');
	    /** @var Admin $admin1 */
		$admin1 = $this->getReference('admin1');

		$ticket = Ticket::createTicket(Uuid::uuid4(), $user1, 'Primo messaggio');
	    $ticket->assign($admin1);
		$ticket->addMessage('Risposta admin', $admin1);
		$this->ticketRepository->add($ticket);

	    $ticket = Ticket::createTicket(Uuid::uuid4(), $user2, 'ticket nuovo');
	    $this->ticketRepository->add($ticket);
    }

	public function getOrder()
	{
		return 20;
	}
}
