<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\User\Admin;
use App\Domain\User\Registered;
use App\Domain\User\SenderPreference;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture implements OrderedFixtureInterface
{
	private $passwordEncoder;

	public function __construct(UserPasswordEncoderInterface $passwordEncoder)
	{
		$this->passwordEncoder = $passwordEncoder;
	}

	public function load(ObjectManager $manager)
    {

        for($i = 1; $i <= 3; $i++) {
	        $user  = Registered::create(Uuid::uuid4(), 'utente'.$i.'@example.com', 'utentepass', $this->passwordEncoder);
	        $admin = Admin::create(Uuid::uuid4(), 'admin'.$i.'@example.com', 'adminpass', $this->passwordEncoder);
			$user->updateApiToken('utente'.$i.'token');
			$admin->updateApiToken('admin'.$i.'token');
	        $this->addReference('utente'.$i, $user);
	        $this->addReference('admin'.$i, $admin);

	        $manager->persist($user);
	        $manager->persist($admin);
        }

        //add some "random" notification preference
	    $user1 = $this->getReference('utente1');
        $user1->addSenderPreference(SenderPreference::PUSH_TYPE);

	    $user2 = $this->getReference('utente2');
	    $user2->addSenderPreference(SenderPreference::SMS_TYPE);

	    $admin1= $this->getReference('admin1');
	    $admin1->addSenderPreference(SenderPreference::PUSH_TYPE);
	    $admin1->addSenderPreference(SenderPreference::SMS_TYPE);

	    $admin2 = $this->getReference('admin2');
	    $admin2->addSenderPreference(SenderPreference::PUSH_TYPE);

        $manager->flush();
    }


	public function getOrder()
	{
		return 10;
	}
}
