<?php

namespace App\Infrastructure\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AdminListType extends AbstractType
{
	public function buildForm( FormBuilderInterface $builder, array $options ) {

		$admins = $options['admins'];
		$choices = [];
		foreach($admins as $admin){
			$choices[$admin->getName()] = $admin->getId();
		}

		$builder->add('admin', ChoiceType::class, [
			'choices' => $choices,
			'multiple' => false,
			'expanded' => false,
			'choice_label' => null
		])->add('save', SubmitType::class);
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => null
		]);
		$resolver->setRequired('admins');
	}
}