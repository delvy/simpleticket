<?php

namespace App\Infrastructure\FormEntity;

use Symfony\Component\Validator\Constraints as Assert;

class Message
{

	/** @Assert\NotBlank() */
	public $body;

	/**
	 * @return mixed
	 */
	public function getBody()
	{
		return $this->body;
	}


}